import difflib  # 导入查重包
import argparse  # 导入命令行包
import os.path

# 命令行输入文件路径
parser = argparse.ArgumentParser(description='命令行传入文件路径')
parser.add_argument('path1', type=str, help='原文文件')
parser.add_argument('path2', type=str, help='抄袭版的论文文件')
parser.add_argument('path3', type=str, help='答案文件')
paths = parser.parse_args()

# 读取path1和path2的文件
if os.path.isfile(paths.path1):
    with open(paths.path1, 'r', encoding='utf-8') as file1:
        a = file1.read()
else:
    print('path1的路径错误或不存在')
if os.path.isfile(paths.path2):
    with open(paths.path2, 'r', encoding='utf-8') as file2:
        b = file2.read()
else:
    print('path2的路径错误或不存在')

if os.path.isfile(paths.path1) & os.path.isfile(paths.path2):
    # 创建一个SequenceMatcher对象
    sm = difflib.SequenceMatcher(None, a, b)

    # 计算并打印两篇文章的相似度
    with open(paths.path3, 'w', encoding='utf-8') as fp:
        similarity = sm.ratio()
        print('差异报告：', file=fp)
        print(f'相似度：{similarity:.2f}', file=fp)
        fp.close()
